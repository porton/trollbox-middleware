// this function detects most providers injected at window.ethereum
// This does not work when this is imported as .js:
// import detectEthereumProvider from 'node_modules/@metamask/detect-provider/dist/index.js'
// import Web3 from 'node_modules/web3/dist/web3.min.js'
const detectEthereumProvider = require('@metamask/detect-provider/dist/index')
const Web3 = require('web3/dist/web3.min')
const truffleContract = require('@truffle/contract')

console.log('Using web3 version: ', Web3.version)

const ethereum = (window as any).ethereum

if (!ethereum) {
  console.warn('Missing ethereum object in the global scope')
}

const defaultEthereumProviders = {
  1: process.env.MAINNET_PROVIDER_URL
    ? process.env.MAINNET_PROVIDER_URL
    : 'wss://mainnet.infura.io/ws/v3/044bfbb71eeb4452a66feb7768d7a1b8', // 'ws://localhost:8545'

  // There should be preferrably wss:// connection instead. You may need to run your own BSC node.
  56: process.env.BSC_PROVIDER_URL
    ? process.env.BSC_PROVIDER_URL
    : 'https://bsc-dataseed.binance.org/' // There are several for your choice: https://docs.binance.org/smart-chain/developer/rpc.html
}

let currentAccount = null

export class Ethereum {
  ethereumProviders = null

  oldNetworkVersion = null
  oldReadWeb3 = null

  writeWeb3 = null

  constructor (ethereumProviders = defaultEthereumProviders) {
    this.ethereumProviders = ethereumProviders
  }

  async onLoad () {
    const provider = await detectEthereumProvider()
  
    if (provider !== ethereum) {
      console.error('Do you have multiple wallets installed?')
    }
  
    // From now on, this should always be true:
    // provider === ethereum
    if (provider) {
      await connectWallet()
  
      this.writeWeb3 = new Web3(provider, {
        timeout: 30000, // ms
        reconnect: {
          auto: true,
          delay: 5000, // ms
          maxAttempts: 5,
          onTimeout: false
      }})
  
      dispatchEvent(EVENTS.NETWORK_CHANGE, this.writeWeb3)
    } else {
      this.writeWeb3 = this.readWeb3()
  
      dispatchEvent(EVENTS.NETWORK_CHANGE, this.writeWeb3)
    }
  }

  async metamaskWrongNetwork () {
    const expected = await this.readWeb3()?.eth?.net?.getId()
        , current = await this.writeWeb3?.eth?.net?.getId()
  
    return {
      isOk: expected === current,
      expected,
      current
    }
  }

  readWeb3 () {
    const networkVersion = ((window as any).ethereum as any).networkVersion
    if (networkVersion === this.oldNetworkVersion && this.oldReadWeb3) {
      return this.oldReadWeb3
    }
    this.oldNetworkVersion = networkVersion
  
    const providerURL = this.ethereumProviders[networkVersion]
    if (!providerURL) return null
  
    this.oldReadWeb3 = new Web3(getProvider(providerURL))
    return this.oldReadWeb3
  }

  async getNetworkId () {
    return await this.readWeb3().eth.net.getId()
  }

  async getContract (platform, contract) {
    if (!contracts[platform][contract.contractName]) {
      if (platform === 'write') {
        const t = truffleContract(contract)
  
        t.setProvider(this.writeWeb3.currentProvider)
        contracts[platform][contract.contractName] = await t.deployed()
      } else if (platform === 'read') {
        const t = truffleContract(contract)
  
        t.setProvider(this.readWeb3().currentProvider)
        contracts[platform][contract.contractName] = await t.deployed()
        // console.log(contract.contractName, contracts[platform][contract.contractName])
      }
    }
    const c = contracts[platform][contract.contractName]
    //  console.log('acquiring contract', contract.contractName, 'at address', c.address)
  
    return c
  }

  async getWeb3Contract (contractAbi) {
    const networkId = await this.getNetworkId()
        , contractAddress = contractAbi.networks[networkId].address
        , readContract = new (this.readWeb3().eth.Contract)(contractAbi.abi, contractAddress)
  
    return readContract
  }
  
  async getReadContractByAddress (contract, address) {
    const t = truffleContract(contract)
  
    t.setProvider(this.readWeb3().currentProvider)
    const readContract = await t.at(address)
  
    return readContract
  }

  async getAllEvents (
    callback,
    contractAbi,
    eventName,
    filter,
    fromBlock = 0,
    toBlock = 'latest',
    eventSignature,
    eventInputs
  ) {
    const readContract = await this.getWeb3Contract(contractAbi)
        , truffleContract = await this.getContract('read', contractAbi)
  
    readContract.getPastEvents(eventName, {
      filter: filter,
      fromBlock: fromBlock,
      toBlock: toBlock
    }, (error, events) => {
      if (error) {
        console.error(error)
        return
      }
  
      for (var i = 0; i < events.length; i++) {
        const event = events[i]
  
        event.returnValues.txHash = event.transactionHash
        callback(event.returnValues)
      }
    })
    this.subscribeLogEvent(truffleContract, eventName, eventSignature, eventInputs, (event) => {
      callback(event)
    })
  }
  
  subscribeLogEvent (contract, eventName, eventSig, eventInputs, callback) {
    this.readWeb3().eth.subscribe('logs', {
      address: contract.address,
      topics: [ eventSig ],
      fromBlock: 'latest' // https://ethereum.stackexchange.com/a/34693/36438
    }, (error, result) => {
      if (!error) {
        console.log('result', result)
        const eventObj = this.readWeb3().eth.abi.decodeLog(
          eventInputs,
          result.data,
          result.topics.slice(1)
        )
  
        eventObj.txHash = result.transactionHash
        callback(eventObj)
      } else {
        console.error(`Error making subscription to ${eventName} on ${contract.address}: ${error}`)
      }
    })
  }
  
  async isUserManagement (contract) {
    let isManagement = false
  
    if (this.writeWeb3) {
      const accounts = await this.writeWeb3.eth.getAccounts()
          , metamaskAddr = accounts[0]
          , managementAddr = await this.getManagementAddress(contract)
  
      isManagement = metamaskAddr === managementAddr
    }
    return isManagement
  }
  
  private async getManagementAddress (contract) {
    const c = await this.getContract('read', contract)
        , mgmt = await c.management()
  
    return mgmt
  }
}

const listeners = {}
    , EVENTS = {
      NETWORK_CHANGE: 'networkChange'
    }

function dispatchEvent (eventName, ...args) {
  const handlers = listeners[eventName]

  if (handlers) {
    handlers.forEach(handler => handler(...args))
  }
}

function addEventListener (eventName, handler) {
  let handlers = listeners[eventName]

  if (!handlers) {
    handlers = listeners[eventName] = []
  }

  handlers.push(handler)
}

function removeEventListener (eventName, handler) {
  const handlers = listeners[eventName]

  if (handlers) {
    const index = handlers.indexOf(handler)

    handlers.splice(index, 1)
  }
}

export default {
  EVENTS,
  addEventListener,
  removeEventListener
}

export async function connectWallet () {
  await ethereum.request({ method: 'eth_requestAccounts' })
    .then(handleAccountsChanged)
    .catch((err) => {
      if (err.code === 4001) {
        // EIP-1193 userRejectedRequest error
        // If this happens, the user rejected the connection request.
        console.log('Please connect to MetaMask.')
      } else {
        console.error(err)
      }
    })
}

// For now, 'eth_accounts' will continue to always return an array
function handleAccountsChanged (accounts) {
  if (accounts.length === 0) {
    // MetaMask is locked or the user has not connected any accounts
    currentAccount = null
  } else if (accounts[0] !== currentAccount) {
    currentAccount = accounts[0]
    // Do any other work!
  }
}

export function isWalletConnected () {
  return currentAccount != null
}

export async function getEthAccount (index = 0) {
  if (!currentAccount) {
    await connectWallet()
    if (!currentAccount) {
      const msg = 'Please ensure that metamask or another browser wallet is installed and activated to use this feature'

      console.error(msg)
      alert(msg)
      throw msg
    }
  }
  return currentAccount
}

/**********************************************************/
/* Handle chain (network) and chainChanged (per EIP-1193) */
/**********************************************************/

// Normally, we would recommend the 'eth_chainId' RPC method, but it currently
// returns incorrectly formatted chain ID values.
// const currentChainId = ethereum?.chainId

ethereum?.on('chainChanged', handleChainChanged)
ethereum?.on('accountsChanged', handleChainChanged)

// TODO: Should it be made optional? (for the case if we only read blockchain)
function handleChainChanged (_chainId) {
  // We recommend reloading the page, unless you must do otherwise
  window.location.reload()
}

const contracts = { read: {}, write: {} }

export function getProvider (url) {
  const provider = /^http/.test(url)
    ? new Web3.providers.HttpProvider(url)
    : new Web3.providers.WebsocketProvider(url)

  return provider
}

export function bytesToString (bytes) {
  return Web3.utils.hexToString(bytes)
}

export function toChecksumAddress (address) {
  return Web3.utils.toChecksumAddress(address)
}

export function fromWei (num) {
  return Web3.utils.fromWei(num)
}

export function toWei (num) {
  return Web3.utils.toWei(num)
}

export function toBN (num) {
  return Web3.utils.toBN(num)
}

export function sha3 (arg) {
  return Web3.utils.sha3(arg)
}
